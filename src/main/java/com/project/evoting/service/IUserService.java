/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.evoting.service;

import com.project.evoting.dto.UserInfoDTO;
import javax.ejb.Local;

/**
 *
 * @author khair.ali
 */
@Local
public interface IUserService {
    
    
    UserInfoDTO authenticate(String userId,String password);
    
    
}
