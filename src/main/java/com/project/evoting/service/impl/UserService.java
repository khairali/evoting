/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.evoting.service.impl;

import com.project.evoting.dao.IUserInfoDAO;
import com.project.evoting.dto.UserInfoDTO;
import com.project.evoting.entity.UserInfo;
import com.project.evoting.mapper.UserInfoMapper;
import com.project.evoting.service.IUserService;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author khair.ali
 */
@Stateless
public class UserService implements IUserService {

    @Inject
    UserInfoMapper userInfoMapper;

    @Inject
    IUserInfoDAO userInfoDAO ;

    @Override
    public UserInfoDTO authenticate(String userId, String password) {
        UserInfo userInfo = userInfoDAO.findById(UserInfo.class, 1);
        return userInfoMapper.mapToDTO(userInfo);
    }

}
