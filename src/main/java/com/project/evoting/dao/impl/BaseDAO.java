/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.evoting.dao.impl;

import com.project.evoting.dao.IBaseDAO;
import com.project.evoting.entity.GenericEntity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author khair.ali
 */
public abstract class BaseDAO<T extends GenericEntity>  implements IBaseDAO<T> {

    
    @PersistenceContext
    EntityManager entityManager;

    @Override
    public T findById(Class<T> c, Integer id) {
        System.out.print(entityManager);
        return entityManager.find(c, id);
    }

    @Override
    public void remove(T t) {
        entityManager.remove(t);
    }

    @Override
    public T saveOrUpdate(T t) {
        return entityManager.merge(t);
    }
    

       

}
