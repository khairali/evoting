/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.evoting.dao;

/**
 *
 * @author khair.ali
 */
public interface IBaseDAO<T> {

    public T findById(Class<T> c, Integer id);

    public void remove(T t);

    public T saveOrUpdate(T t);

}
