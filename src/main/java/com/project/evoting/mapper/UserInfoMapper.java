/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.evoting.mapper;

import com.project.evoting.dto.UserInfoDTO;
import com.project.evoting.entity.UserInfo;
import javax.enterprise.inject.Default;

/**
 *
 * @author khair.ali
 */
public class UserInfoMapper extends GenericMapper<UserInfo,UserInfoDTO> {

    @Override
    public UserInfo mapToEntity(UserInfoDTO src) {
        UserInfo dest = null;
        if(null != src){
         dest = new UserInfo();
         dest.setId(src.getId());
         dest.setFirstName(src.getFirstName());
         dest.setLastName(src.getLastName());
         dest.setEmail(src.getEmail());
         dest.setAge(src.getAge());
        }
        return dest;
    }

    @Override
    public UserInfoDTO mapToDTO(UserInfo src) {
        UserInfoDTO dest = null;
        if(null != src){
         dest = new UserInfoDTO();
         dest.setId(src.getId());
         dest.setFirstName(src.getFirstName());
         dest.setLastName(src.getLastName());
         dest.setEmail(src.getEmail());
         dest.setAge(src.getAge());
        }
        return dest;
    }

}
