/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.project.evoting.mapper;

import com.project.evoting.dto.GenericDTO;
import com.project.evoting.entity.GenericEntity;

/**
 *
 * @author khair.ali
 */
public abstract class GenericMapper<T extends GenericEntity,R extends GenericDTO > {
    
    
    
    public abstract T mapToEntity(R src);
    
    public abstract R mapToDTO(T src);
    
}
